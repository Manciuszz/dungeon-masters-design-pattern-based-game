# Dungeon Masters...

### Apie žaidimą... ###

* Požemių valdovas - tai vaidmenų​ ​ tipo​ ​ žaidimas, kuriame bandai ištrūkti iš požemio į paviršių. 
* Žaidimo pradžioje, mes esame nukeliami į pagrindinį žaidimo meniu (“Start Menu”), kuriame turėsime galimybę pradėti žaisti žaidimą (“Start Game”) arba išeiti iš žaidimo (“Exit Game”).
* Tiek pačio žaidimo bei meniu navigacija yra atliekama klaviatūros klavišais W,A,S,D bei ENTER - iškviesti meniu arba patvirtinti mūsų sprendimą.
* Pasirinkus pradėti žaisti žaidimą (“Start Game”), mes valdome mūsų herojų - “Alpha Male”, kuris yra nukeliamas į požėmį. 
* Požėmių lygiai yra kiekvieną kartą skirtingi, todėl teks gerai paklaidžioti. 
* Taip pat, skersai stoti kelią gali įvairūs monstrai, štai čia, mes stosime į kovą, kuri vyks pakaitomis (pulsime/gynsimes), mūsų pergalė nulems are šie monstrai išmes įvairius daiktus, kuriais mūsų herojus gali pasistiprinti save.
* Požėmis yra sudarytas iš simbolių, kurie reiškia: 
	❖ # - požėmio siena 
	❖ . - vaikštomasis kelias 
	❖ ^ - kelias į auktštesnį lygį 
    ❖ @ - mūsų herojus 
    ❖ D - durys 
    ❖ M - monstras … ir t.t. 
* Žaidėjas taip pat gali atsidaryti mini žemėlapį (Klaviatūros klavišas M), kuris parodys jau aplankytas vietas. 
 
### Kaip pradėti žaisti? ###

* Sukompiliuok.
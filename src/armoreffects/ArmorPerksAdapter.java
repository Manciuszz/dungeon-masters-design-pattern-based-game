package armoreffects;

import character.Figure;

public class ArmorPerksAdapter implements ArmorEffect {
    
    private Perks perk;
    
    public ArmorPerksAdapter( Perks perk ) {
        this.perk = perk;
    }

    @Override
    public void action(Figure target) {}
    
    @Override
    public String toString() {
        return "Perk: " + this.perk;
    }
}

package dungeon;


import gameEngine.GameEngine;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.util.Random;
import Display.*;

public class NullRoom extends AbstractRoom{
	private int height;
	private int width;
	private int doors[];
	private int doorNumber;
	private boolean isEndRoom;
	private Dimension position;
	private Panel panels[][];
	
	public NullRoom(int height, int width, int doorNumber, int[] doors)
	{
		this.height = height;
		this.width = width;
		this.doorNumber = doorNumber;
		this.doors = doors;
		this.isEndRoom = false;
		this.panels = new Panel[this.height][this.width];
		this.generateRoom();
	}
	
	protected void generateRoom()
	{
		System.out.println("Oops! Room doesn't exist.");
	}
	
	public Dimension getPosition(){return this.position;}
	public boolean isEndRoom(){return this.isEndRoom;}
	
	public boolean setEndRoom()
	{
		return this.isEndRoom;
	}
	
	public void enterRoom(int direction)
	{
		
		if(direction < 0)
			this.position = new Dimension(this.height/2, this.width/2);
		else
		{
			switch(direction)
			{
			case 0:
				this.position = new Dimension(this.height - 2, this.width/2);
				break;
			case 1:
				this.position = new Dimension(1, this.width/2);
				break;
			case 2:
				this.position = new Dimension(this.height/2, 1);
				break;
			case 3:
				this.position = new Dimension(this.height/2, this.width - 2);
			}
		}
		this.open();
	}
	
	public int getDoorNumber(){return this.doorNumber;}
	public int getHeight(){return this.height;}
	public int getWidth(){return this.width;}
	public int[] getDoors(){return this.doors;}
	public void setPanels(Panel[][] room){this.panels = room;}
	public String displayString()
        {
		String room = "Room doesn't exist";
		
		return room;
	}
	
	public boolean update(KeyEvent e, boolean focused)
	{
		return false;
	}

	public String eventString() {
		return "";
	}

	public void open() {
		Display.getInstanceOfDisplay().setFocus(this);		
	}

	public String getName() {
		return "NullRoom";
	}
        
        public boolean isNil() {
            return true;
        }
}
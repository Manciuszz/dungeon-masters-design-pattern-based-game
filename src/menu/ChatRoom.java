package menu;

import character.Figure;
import java.util.Date;

public class ChatRoom {
    private static final Caretaker caretaker = new Caretaker();
    private static final Originator originator = new Originator();
    
    public static void showMessage(Figure user, String message) {        
        System.out.println(new Date().toString() + " [" + user.getName() + "] : " + message);   
        lastMessage(message);
    }
    
    public static void publicAnnouncementHighlight() {
        System.out.println("[Public Announcement]");      
        originator.restore( caretaker.getMemento() );
    }
    
    private static void lastMessage(String message) {
        originator.setState(message);  
    }
    
    public static void saveState() {
        System.out.println("");
        caretaker.addMemento( originator.save() );
    }
    
}
